/* 	Nifty aspect of input-output pair:

	This test not only includes an example of polymorphism
	(with the "test" method in class S), but also includes
	an instance of shadowing which may prove tricky to deal
	with for a call-graph generating program.
	
	Class S contains a field named "a" of type A which is
	shadowed in method "test" by a parameter named "a" of 
	type A. However, in this program, the "test" method is 
	only called on an instance of type B (extends A), which 
	exercises polymorphism. A correct call-graph generating 
	program would not mistake method calls through this 
	parameter to be through the field instead.
*/

class Main {
	public static void main(String[] args) {
		S s;
		boolean b;

		s = new S();
		b = s.test(new B());
	}
}

class A {
	public boolean m() {
		return false;
	}
}

class B extends A {
	public boolean m() {
		return true;
	}
}

class S {
	A a;

	public boolean test(A a) {
		return a.m();
	}
}
