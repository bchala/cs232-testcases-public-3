/*Example IO pair for HW1: performing some simple actions with imaginary machines; checking their on/off status, turning them on, checking their battery levels, using up their energy
reserves. Call graph points out the recursive nature of the "battery use" function useMax and what goes into it and all other defined function. */
class IOExample {
	public static void main(String[] a) {
		System.out.println(new Machine().cycle());
	}
}

class Machine {
	boolean battery;
	boolean usage;
	boolean shutdown;
	int onstatus;
	boolean os;
	
    public int cycle(){
        onstatus = 1;
		battery = this.isBatteryCritical(40);
		usage = this.useMax(40);
		os = this.isOff(onstatus);
        return onstatus;
    }
	
	public int subtract(int a, int b) {
		return a - b;
	}
	
	public boolean isLess(int a, int b) {
		return a < b;
	}
	
	public boolean isBatteryCritical(int b) {
		return this.isLess(15, b);
	}

	public boolean useMax(int b) {
		boolean done;
		if (0 < b) 
			place = this.useMax(this.subtract(b, 1));
		else
			done = true;
		return done;
	}
	
	public boolean isOff(int s) {
		return this.isLess(s, 1);
	}
}